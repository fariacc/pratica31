/**
 * @author Carolina Cordeiro Faria
 * RA: 1861735
 */
import java.util.Date;
import java.util.GregorianCalendar;

public class Pratica31 {

    private static String meuNome = "Carolina Cordeiro Faria";
    private static GregorianCalendar dataNascimento;
    private static Date inicio;
    private static Date fim;

    public static void main(String[] args) {
        inicio = new Date();

        System.out.println(meuNome.toUpperCase());
        System.out.println(meuNome.substring(18, 19).toUpperCase() + meuNome.substring(19, 23).toLowerCase() + ", " + meuNome.toUpperCase().charAt(0) + ". " + meuNome.toUpperCase().charAt(9) + ".");

        dataNascimento = new GregorianCalendar(1994, 10 /* 0-janeiro 11-dezembro */, 21);
        System.out.println((new GregorianCalendar().getTime().getTime() - dataNascimento.getTime().getTime()) / (86400000));

        fim = new Date();
        System.out.println(fim.getTime() - inicio.getTime());
        }  
}